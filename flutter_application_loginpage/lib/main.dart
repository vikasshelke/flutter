import "package:flutter/material.dart";
import "package:flutter_application_loginpage/homepage.dart";
import "package:flutter_application_loginpage/loginpage.dart";
import "package:google_fonts/google_fonts.dart";

void main()
{
  runApp(RoutesApp());
}

class RoutesApp extends StatelessWidget {
  const RoutesApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      themeMode: ThemeMode.light,
      theme: ThemeData(primarySwatch: Colors.deepOrange,fontFamily: GoogleFonts.lato().fontFamily),
      darkTheme: ThemeData(brightness: Brightness.dark),

      initialRoute: "/home",
      routes: {

        "/"     : (context) => LogInPage(),
        "/home" : (context) => HomePage(),
        "/login": (context) => LogInPage(),
      }
    );
  }
}