import "package:flutter/material.dart";
import "package:flutter_application_routes/homepage.dart";
import "package:flutter_application_routes/loginpage.dart";

void main()
{
  runApp(RoutesApp());
}

class RoutesApp extends StatelessWidget {
  const RoutesApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      themeMode: ThemeMode.light,
      theme: ThemeData(primarySwatch: Colors.deepOrange),
      darkTheme: ThemeData(brightness: Brightness.dark),

      initialRoute: "/login",
      routes: {

        "/"     : (context) => LogInPage(),
        "/home" : (context) => HomePage(),
        "/login": (context) => LogInPage(),
      }
    );
  }
}