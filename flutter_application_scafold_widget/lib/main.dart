import 'package:flutter/material.dart';
import 'package:flutter_application_scafold_widget/homepage.dart';

void main()
{
  runApp(const SimpleTextApp());
}

class SimpleTextApp extends StatelessWidget
 {
  const SimpleTextApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) 
  {
    return const MaterialApp(
      home:homepage(),
    ); 
  }
}